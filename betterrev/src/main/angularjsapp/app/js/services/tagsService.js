/**
 * A service factory which creates an object with methods for fetching 
 * tags from the REST service.
 */
betterrevApp.factory('tagsService', ['$resource', 'environment', function ($resource, environment) {
     return $resource(environment.baseURL + 'betterrev/admin/tags/:tagId', {tagId: '@id'}, {
         update: {
             method: 'PUT'
         }
     });
}]);

