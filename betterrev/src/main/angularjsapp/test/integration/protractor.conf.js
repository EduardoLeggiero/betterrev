exports.config = {
    
    framework: 'mocha',
    seleniumServerJar: '../../node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-2.45.0.jar',
    chromeDriver: '../../node_modules/chromedriver/bin/chromedriver',
    // The address of a running selenium server.
    //seleniumAddress: 'http://localhost:4444/wd/hub',

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        'browserName': 'chrome'
    },
    // Spec patterns are relative to the location of the conf file. They may
    // include glob patterns.
    specs: ['./**/*spec.js'],
    
    // Mocha options
    mochaOpts: {
        reporter: 'spec',
        bail: true,
        slow: 10000
    }
};
