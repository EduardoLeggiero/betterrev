var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('ContactUs Page Tests', function() {

    // Look for the id of "contactusHeading"
    it('ContactUs Page exists', function() {
        this.timeout(50000);
        browser.get('http://127.0.0.1:8090/#/contactus');
        expect(element(protractor.By.id("contactusHeading")).isDisplayed()).to.eventually.equal(true);
    });

});
