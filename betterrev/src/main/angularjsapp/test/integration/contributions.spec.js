var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Contributions Page Tests', function() {

    // Look for the id of "contributionsTable"
    it('Contributions Table exists', function() {
        this.timeout(50000);
        browser.get('http://127.0.0.1:8090/#/contributions');
        expect(element(protractor.By.id("contributionsTable")).isDisplayed()).to.eventually.equal(true);
    });

});
