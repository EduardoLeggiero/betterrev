var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Home Page Tests', function () {

  // Look for the ids of "homeHeading", "homeWorkflow", "homeShowContributions"
  it('Home Page exists', function () {
      this.timeout(50000);
      browser.get('http://127.0.0.1:8090/#/index');
      expect(element(protractor.By.id("homeHeading")).isDisplayed()).to.eventually.equal(true);
      expect(element(protractor.By.id("homeWorkflow")).isDisplayed()).to.eventually.equal(true);
      expect(element(protractor.By.id("homeShowContributions")).isDisplayed()).to.eventually.equal(true);
  });

});
