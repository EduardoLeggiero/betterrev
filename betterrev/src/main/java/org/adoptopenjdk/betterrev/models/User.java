package org.adoptopenjdk.betterrev.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * User entity class, which represents all Users within the BetterRev
 * application.
 */
@Entity
@Table(name = "betterrev_user")
@NamedQueries({
    @NamedQuery(name = "User.findUserById",
            query = "SELECT u FROM User u WHERE u.id = :userId")
})
public class User implements Serializable {

  /**
   * @return the bitbucketUserName
   */
  public String getBitbucketUserName() {
    return bitbucketUserName;
  }

  /**
   * @param bitbucketUserName the bitbucketUserName to set
   */
  public void setBitbucketUserName(String bitbucketUserName) {
    this.bitbucketUserName = bitbucketUserName;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the ocaStatus
   */
  public OcaStatus getOcaStatus() {
    return ocaStatus;
  }

  /**
   * @param ocaStatus the ocaStatus to set
   */
  public void setOcaStatus(OcaStatus ocaStatus) {
    this.ocaStatus = ocaStatus;
  }

    public enum OcaStatus {
        AWAITING_APPROVAL,
        SIGNED, 
        NOT_SIGNED, 
        UNKNOWN;
        
        public String displayName() {
            return this.name().replaceAll("_", " ").toLowerCase();
        }
    }

    public User() {}
    
    // TODO get by ID the Java EE Way
    //public static Model.Finder<Long, User> find = new Model.Finder<>(Long.class, User.class);

    // TODO do the Java EE Way
    /*
    public static User findOrCreate(String bitbucketUserName, String displayName) {
        User user = find.where().eq("bitbucketUserName", bitbucketUserName).findUnique();
        if (user == null) {
            user = new User(bitbucketUserName, displayName, OcaStatus.UNKNOWN);
            user.save();
        }
        return user;
    }
    */
    public static User findOrCreate(String bitbucketUserName, String displayName) {
        return null;
    }
    
    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String bitbucketUserName;

    private String openjdkUsername;

    @Enumerated(EnumType.STRING)
    private OcaStatus ocaStatus;

    private LocalDateTime createdDate;

    public User(String bitbucketUserName, String name, OcaStatus ocaStatus) {
        this.bitbucketUserName = bitbucketUserName;
        this.name = name;
        this.ocaStatus = ocaStatus == null ? OcaStatus.UNKNOWN : ocaStatus;
        this.createdDate = LocalDateTime.now();
    }

    @Override
    public int hashCode() {
        return getBitbucketUserName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        User other = (User) obj;
        return Objects.equals(getBitbucketUserName(), other.getBitbucketUserName());
    }

    public Long getId() {
        return id;
    }

    // Remove later
    public void setKey(Long key) {
        id = key;
    }
}
