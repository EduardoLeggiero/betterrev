package org.adoptopenjdk.betterrev.admin.controllers;

import org.adoptopenjdk.betterrev.models.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@RequestScoped
@Path("admin/tags")
public class TagsController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TagsController.class);

    @PersistenceContext
    private EntityManager entityManager;

    private Map<Long, Tag> mockTags;

    @PostConstruct
    public void init() {
        this.mockTags = new ConcurrentHashMap<>();
        final Tag tag = new Tag("foobar", "test tag");
        tag.setId(1L);
        this.mockTags.putIfAbsent(1L, tag);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Tag> allTags() {
//        return mockTags.values();
        return entityManager.createNamedQuery("Tag.findAll", Tag.class).getResultList();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    // TODO: Verify transactional
    @Transactional
    public Response createTag(@Valid Tag tag) {
        // TODO get a real contribution
        LOGGER.info("Received request to create a new tag");

//        final long newId = mockTags.size() + 1L;
//        tag.setId(newId);
//        mockTags.put(newId, tag);
        entityManager.persist(tag);
        return Response.noContent().build();
    }

    @Path("{tagId}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    // TODO: Verify transactional
    @Transactional
    public Response updateTag(@PathParam("tagId") Long tagId, @Valid Tag updatedTag) {
        LOGGER.info("Received request to update tag");
        final Optional<Tag> tag = Optional.ofNullable(entityManager.find(Tag.class, tagId));
        if (!tag.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        final Tag retrievedTag = tag.get();
        retrievedTag.setName(updatedTag.getName());
        retrievedTag.setDescription(updatedTag.getDescription());
        entityManager.merge(retrievedTag);
        return Response.noContent().build();
    }

    @Path("{tagId}")
    @DELETE
    // TODO: Verify transactional
    @Transactional
    public Response deleteTag(@PathParam("tagId") Long tagId) {
//        mockTags.remove(tagId);
        final Optional<Tag> tag = Optional.ofNullable(entityManager.find(Tag.class, tagId));
        if (!tag.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        entityManager.remove(tag.get());
        return Response.noContent().build();
    }
}
