package org.adoptopenjdk.betterrev.update;

import akka.actor.UntypedActor;
import akka.event.EventStream;

public abstract class BetterrevActor extends UntypedActor {
    
    public EventStream eventStream() {
        return getContext().system().eventStream();
    }

}
